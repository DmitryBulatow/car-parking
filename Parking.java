
import java.util.ArrayList;
import java.util.Arrays;

public class Parking {

	public ArrayList<Boolean> numberPackagesForLittleCars;
	public ArrayList<Boolean> numberPackagesForBigCars;

	public int placesForLittleCars;
	public int placesForBigCars;
	public int bigCarsGoInThisMove;
	public int littleCarsGoInThisMove;

	public Data data;


	public Parking(int placesForLittleCars, int placesForBigCars){
		this.numberPackagesForLittleCars = new ArrayList<>();
		this.numberPackagesForBigCars = new ArrayList<>();

		this.placesForLittleCars = placesForLittleCars;
		this.placesForBigCars = placesForBigCars;

		for (int place = 0; place < placesForLittleCars; place++) {
			numberPackagesForLittleCars.add(false);
		}

		for (int place = 0; place < placesForBigCars; place++) {
			numberPackagesForBigCars.add(false);
		}


		newClearParking();

		data = new Data(placesForLittleCars, placesForBigCars);
	}


	public int hovManyLittleCarsWantCome(){  // сколько машин заедет за ход
		littleCarsGoInThisMove = ((int) (Math.random() * ((placesForBigCars+placesForLittleCars) /3)));
		return littleCarsGoInThisMove;
	}

	public int hovManyBigCarsWantCome(){  // сколько машин заедет за ход
		bigCarsGoInThisMove = ((int) (Math.random() * ((placesForBigCars+placesForLittleCars) /3)));
		return bigCarsGoInThisMove;
	}

	public void littleCarGo(){  // впихиваем по одной маленькой машине, если место есть

		for (int i = 0; i<littleCarsGoInThisMove;) {
			if(haveFreeLittlePlace()){
				littleCarsGoInThisMove--;
			}
			else{
				i = littleCarsGoInThisMove;
			}
		}

		if (littleCarsGoInThisMove>0){
			haveNotPlaceForLittleCars();
		}

	}


	public void bigCarGo(){  // впихиваем по одной большой машине, если место есть

		for (int i = 0; i<bigCarsGoInThisMove;) {
			if(haveFreeBigPlace()){
				bigCarsGoInThisMove--;
			}
			else{
				i = bigCarsGoInThisMove;
			}
		}

		if (bigCarsGoInThisMove>0){
			haveNotPlaceForBigCars();
		}

	}

	private void haveNotPlaceForBigCars() {
		System.out.println("Sorry, we haven't places for your truck\n" +
				           "The nearest seat will be available in a  " + data.soonLeaveBig() + "  move");
	}

	private void haveNotPlaceForLittleCars() {
		System.out.println("Sorry, we haven't places for your car\n" +
				           "The nearest seat will be available in a  " + data.soonLeaveLittle() + "  move");
	}


	public boolean haveFreeLittlePlace(){    // есть ли свободное место на парковке для маленькой/большой машины

		for (int place = 0; place < numberPackagesForLittleCars.size(); place++) {

			if (!numberPackagesForLittleCars.get(place)){

				numberPackagesForLittleCars.set(place, true);
				data.setCarNumberOnLittle(place);
				data.setCarTimeOnLittle(place);
				data.setCarTypeOnLittle("Car    ", place);
				return true;   // нашлось место)

			}

		}

		return false;  // не нашлось места(
	}

	private boolean haveFreeBigPlace() {  // есть ли свободное место на парковке для большой машины

		for (int place = 0; place < placesForBigCars; place++) {

			if (!numberPackagesForBigCars.get(place)){

				numberPackagesForBigCars.set(place, true);
				data.setCarTimeOnBig(place);
				data.setCarNumberOnBig(place);
				data.setCarTypeOnBig("Truck  ", place);
				return true;   // нашлось место)
			}
		}

		for (int place = 0; place < placesForLittleCars-1; place++) {
			if (!numberPackagesForLittleCars.get(place) && !numberPackagesForLittleCars.get(place + 1)){
				numberPackagesForLittleCars.set(place, true);
				numberPackagesForLittleCars.set(place + 1, true);


				data.setCarTimeOnLittle(place);
				data.setCarTimeOnLittleDouble(place+1);

				data.setCarNumberOnLittle(place);
				data.setCarNumberOnLittleDouble(place+1);

				data.setCarTypeOnLittle("Truck.1", place);
				data.setCarTypeOnLittle("Truck.2", place+1);
				return true;   // нашлось место)

			}
		}
		return false; // не нашлось(
	}

	public void heldWasted(){
		data.motion();
		for (int place = 0; place < placesForLittleCars; place++) {
			if (data.carTimeOnLittle.get(place) == 0){
				numberPackagesForLittleCars.set(place, false);
			}
		}

		for (int place = 0; place < placesForBigCars; place++) {
			if (data.carTimeOnBig.get(place) == 0){
				numberPackagesForBigCars.set(place, false);
			}
		}
	}

	public int getFreePlaceLittle(){
		return data.getFreePlacesLittle();
	}

	public int getNOTFreePlaceLittle(){
		return data.getNOTFreePlacesLittle();
	}

	public int getFreePlaceBig(){
		return data.getFreePlacesBig();
	}

	public int getNOTFreePlaceBig(){
		return data.getNOTFreePlacesBig();
	}

	public void deleteAllCarsFromParking(){
		newClearParking();
		data.deleteAllCarsFromData();
	}

	private void newClearParking(){
		for (int place = 0; place < placesForLittleCars; place++) {
			numberPackagesForLittleCars.set(place, false);
		}

		for (int place = 0; place < placesForBigCars; place++) {
			numberPackagesForBigCars.set(place, false);
		}
	}


	public String getType(int place){
		if (place<placesForLittleCars){
			return "Car  ";
		}
		else{
			return  "Track";
		}
	}



	public String getTypeReal(int place){
		if (place<placesForLittleCars){
			return data.getCarTypeOnLittle().get(place);
		}
		return data.getCarTypeOnBig().get(place - placesForLittleCars);
	}

	public int getNumb(int place){
		if (place<placesForLittleCars){
			return data.getCarNumberOnLittle().get(place);
		}
		return data.getCarNumberOnBig().get(place - placesForLittleCars);

	}

	public int getTime(int place){
		if (place<placesForLittleCars){
			return data.getCarTimeOnLittle().get(place);
		}

		return data.getCarTimeOnBig().get(place - placesForLittleCars);
	}

}
