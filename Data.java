
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Data {

	public ArrayList<Integer> carNumberOnLittle;
	public ArrayList<Integer> carNumberOnBig;
	public ArrayList<Integer> carTimeOnLittle;
	public ArrayList<Integer> carTimeOnBig;
	public ArrayList<String> carTypeOnLittle;
	public ArrayList<String> carTypeOnBig;
	public int placesForLittleCars;
	public int placesForBigCars;
	public Car car = new Car();


	public Data(int placesForLittleCars, int placesForBigCars){
		this.placesForBigCars = placesForBigCars;
		this.placesForLittleCars = placesForLittleCars;
		this.carNumberOnLittle = new ArrayList<>();
		this.carTimeOnLittle = new ArrayList<>();
		for (int place = 0; place < placesForLittleCars; place++) {
			carTimeOnLittle.add(0);
			carNumberOnLittle.add(0);
		}
		this.carNumberOnBig = new ArrayList<>();
		this.carTimeOnBig = new ArrayList<>();

		for (int place = 0; place < placesForBigCars; place++) {
			carTimeOnBig.add(0);
			carNumberOnBig.add(0);
		}
		this.carTypeOnLittle = new ArrayList<>(placesForLittleCars);

		for (int i = 0; i < placesForLittleCars; i++) {
			carTypeOnLittle.add("NO     ");
		}
		this.carTypeOnBig = new ArrayList<>(placesForBigCars);
		for (int i = 0; i < placesForBigCars; i++) {
			carTypeOnBig.add("NO     ");
		}
	}

	public ArrayList<String> getCarTypeOnLittle() {
		return carTypeOnLittle;
	}

	public void setCarTypeOnLittle(String type,int place) {
		carTypeOnLittle.set(place, type);
	}

	public ArrayList<String> getCarTypeOnBig() {
		return carTypeOnBig;
	}

	public void setCarTypeOnBig(String type,int place) {
		carTypeOnBig.set(place, type);
	}

	public ArrayList<Integer> getCarNumberOnLittle() {
		return carNumberOnLittle;
	}

	public void setCarNumberOnLittle(int place) {
		int numb = car.getNumberCur();
		carNumberOnLittle.set(place, numb);
		car.nextNumberCur();
	}

	public void setCarNumberOnLittleDouble(int place) {
		int numb = car.getNumberCur();
		carNumberOnLittle.set(place, numb + 1);
	}

	public ArrayList<Integer> getCarNumberOnBig() {
		return carNumberOnBig;
	}

	public void setCarNumberOnBig(int place) {
		int numb = car.getNumberCur();
		carNumberOnBig.set(place, numb);
		car.nextNumberCur();
	}

	public ArrayList<Integer> getCarTimeOnLittle() {
		return carTimeOnLittle;
	}

	public void setCarTimeOnLittle(int place) {
		car.setNewTimeParking();
		carTimeOnLittle.set(place, car.getTimeParking());
	}

	public void setCarTimeOnLittleDouble(int place){
		carTimeOnLittle.set(place, car.getTimeParking());
	}

	public ArrayList<Integer> getCarTimeOnBig() {
		return carTimeOnBig;
	}

	public void setCarTimeOnBig(int place) {
		car.setNewTimeParking();
		carTimeOnBig.set(place, car.getTimeParking());
	}

	public void motion(){
		for (int place = 0; place < carTimeOnLittle.size(); place++) {
			if (carTimeOnLittle.get(place) >0){
				int temp = carTimeOnLittle.get(place);
				carTimeOnLittle.set(place, temp-1);
			}
		}


		for (int place = 0; place < carTimeOnBig.size(); place++) {
			if (carTimeOnBig.get(place) >0){
				int temp = carTimeOnBig.get(place);
				carTimeOnBig.set(place, temp-1);
			}
		}
	}

	public String soonLeaveLittle(){
		int min = 2147483647;
		boolean wasOrNot = false;
		for (int j : carTimeOnLittle) {
			if (j < min) {
				min = j;
				wasOrNot = true;
			}
		}
		if (wasOrNot){
			return String.valueOf(min);
		}
		return "Never(";
	}

	public String soonLeaveBig(){
		int min = 2147483647;
		boolean wasOrNot = false;
		for (int j : carTimeOnBig) {
			if (j < min) {
				min = j;
				wasOrNot = true;
			}
		}
		for (int i = 0; i < carTimeOnLittle.size()-1; i++) {
			if (min > Math.max(carTimeOnLittle.get(i), carTimeOnLittle.get(i + 1))){
				min = Math.max(carTimeOnLittle.get(i), carTimeOnLittle.get(i + 1));
				wasOrNot = true;
			}
		}
		if (wasOrNot){
			return String.valueOf(min);
		}
		return "Never(";
	}
	
	public int getFreePlacesLittle(){
		int sum =0;
		for (int i : carTimeOnLittle) {
			if (i == 0) {
				sum++;
			}
		}
		return sum;
	}

	public int getFreePlacesBig(){
		int sum =0;
		for (int i : carTimeOnBig) {
			if (i == 0) {
				sum++;
			}
		}
		return sum;
	}

	public int getNOTFreePlacesLittle(){
		int sum =0;
		for (int i : carTimeOnLittle) {
			if (i != 0) {
				sum++;
			}
		}
		return sum;
	}

	public int getNOTFreePlacesBig(){
		int sum =0;
		for (int i : carTimeOnBig) {
			if (i != 0) {
				sum++;
			}
		}
		return sum;
	}

	public void deleteAllCarsFromData(){
		for (int place = 0; place < placesForLittleCars; place++) {
			carTimeOnLittle.set(place, 0);
			carNumberOnLittle.set(place, 0);
		}

		for (int place = 0; place < placesForBigCars; place++) {
			carTimeOnBig.set(place, 0);
			carNumberOnBig.set(place, 0);
		}
		car.setNumberCar(9999);
	}

}
