import java.util.Scanner;


public class Main {

	public static Parking parking;
	public static Scanner sc = new Scanner(System.in);
	public static int placesForLittleCars;
	public static int placesForBigCars;


	public static void main(String[] args) {


		parkingSpace();

		System.out.println("\n");

		while (true) {
			System.out.println("Choose What to do:\n\n" +
							   "go  - Do nex move\n" +
					           "cF  - Check free places\n" +
					           "cNF - Check not free places\n" +
					           "DC  - Delete all car on the parking\n" +
					           "all - check all information about parking\n");

			String toDo = sc.next();
			switch (toDo){
				case ("go"):go();
					break;
				case ("cF"):cFree();
					break;
				case ("cNF"):cNOTFree();
					break;
				case ("all"): allAboutParking();
					break;
				case ("DC"): dellAllCars();
					break;
				default:
					System.out.print("Just error\n" +
							"   404   ");
					break;
			}
			System.out.println("\n");
		}
	}

	private static void allAboutParking() {
		System.out.println("\n");
		for (int i = 0; i < placesForBigCars+placesForLittleCars; i++) {
			String placesFor = parking.getType(i);
			String type = parking.getTypeReal(i);
			String liveTime = String.valueOf(parking.getTime(i));
			String numb = String.valueOf(parking.getNumb(i));


			if (liveTime.equals("0")){
				numb =     "-   ";
				liveTime = "-";
				type =     "NO     ";
			}
			String number = number(i+1);
			System.out.println(number + ")  Intended for: " + placesFor + "   |   Located: " + type + "    |    Numb: " + numb + "   |    Live Time: " + liveTime + "  |");
		}
	}

	private static String number(int i) {
		int sumPlaces = placesForBigCars+placesForLittleCars;
		String number = String.valueOf(i);

		int k= 10;
		while (sumPlaces>=k){
			if (i<k){
				number = "0" + number;
				sumPlaces /= 10;
			}
			k *= 10;
		}

		return  number;
	}

	private static void parkingSpace() {
		System.out.print("number of Parking spaces in your car Park\n" +
				         "for little cars = ");
		placesForLittleCars = sc.nextInt();
		System.out.print("\nfor big cars = ");
		placesForBigCars = sc.nextInt();
		parking = new Parking(placesForLittleCars, placesForBigCars);
	}

	private static void cFree() {
		System.out.println(parking.getFreePlaceLittle() + " for car\n" +
				           parking.getFreePlaceBig() + " for truck");
	}

	private static void cNOTFree() {
		System.out.println(parking.getNOTFreePlaceLittle() + " for car\n" +
				           parking.getNOTFreePlaceBig() + " for truck");
	}

	private static void dellAllCars() {
		parking.deleteAllCarsFromParking();
		System.out.println("\n<THANOS_SNAP>");
	}

	private static void go() {

		parking.heldWasted();

		int cars = parking.hovManyLittleCarsWantCome();

		int trucks = parking.hovManyBigCarsWantCome();

		parking.littleCarGo();

		parking.bigCarGo();

		System.out.println("\nMove was did");

		System.out.println("Cars were driven over this turn: " + cars);

		System.out.println("Trucks stopped for this move: " + trucks);
//todo сколько машин заехало

	}

}
